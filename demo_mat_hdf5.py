def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    d = loadmat(infile)

    return d

def read_hdf5(infile):
    """read HDF5 input

    :param infile: input file (str)
    :return: mat_file_ref
    """
    import h5py

    f = h5py.File(infile)

    keys = [key for key in f.keys()]

    for k in keys[2:]:
        print(k)
        print(f.get(k))
    print([key for key in f.keys()])
    
    return dict(f)

def read_any_mat(infile):
    try:
        outfile = read_mat(infile)
    except NotImplementedError:
        outfile = read_hdf5(infile)
    return outfile

if __name__ == "__main__":
    print(read_any_mat('mat_v73.mat'))